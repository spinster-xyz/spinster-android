# Spinster Android Client

This is the Android client for [Spinster](https://spinster.xyz).

Spinster Android is a fork of Tusky, which is a client for [Mastodon](https://github.com/tootsuite/mastodon). Mastodon is an ActivityPub federated social network. That means no single entity controls the whole network, rather, like e-mail, volunteers and organisations operate their own independent servers, users from which can all interact with each other seamlessly.

[<img src="/assets/fdroid_badge.png" alt="Get it on F-Droid" height="80" />](https://f-droid.org/repository/browse/?fdid=xyz.spinster.client.fdroid)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" alt="Get it on Google Play" height="80" />](https://play.google.com/store/apps/details?id=xyz.spinster.client)

## Features

- Material Design
- Most Mastodon APIs implemented
- Multi-Account support
- Light, dark, and black themes with the possibility to auto-switch based on the time of day
- Drafts - compose posts and save them for later
- Choose between different emoji styles
- Optimized for all screen sizes
- Completely free software - no non-free dependencies like Google services

### Support

If you have any bug reports, feature requests or questions please open an issue or message us at [spinster@spinster.xyz](https://spinster.xyz/@spinster)!

### Head of development

This fork is maintained by [socjuswiz@spinster.xyz](https://spinster.xyz/@socjuswiz).

### 
